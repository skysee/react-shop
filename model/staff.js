var ActiveRecord = require('active_record');
/* Exports the module */
module.exports = Staff
/* Extends the module to ActiveRecord.Base */
ActiveRecord.Base.extend(Staff, ActiveRecord.Base)
/* Create the Class */
function Staff (){
    /* Initialize the instance variables */
    this.initialize(arguments[0]);
}
/* Configure the model */
Staff.table_name = 'staff';
Staff.fields('email'); // Create dynamics finders: User.find_by_email, etc.
