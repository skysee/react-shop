var Footer = React.createClass({
    render: function() {
        return (
            <div className="footer">
                <p className="copyright">Copyright@Leon Zhou 2015</p>.
            </div>
        );
    }
});

React.render(
    <Footer/>,
    document.getElementById('footer')
);