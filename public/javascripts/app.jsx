var Layout = React.createClass({

    render: function () {
        return (
            <div>
                <div id="header"></div>
                <div className="nav_bar_top" id="nav_bar_top"></div>
                <div className="content" >
                    <div className="nav_bar_left" id="nav_bar_left"></div>
                    {this.props.children}

                </div>
                <div className="clear" ></div>
                <div id="footer"></div>

            </div>
        );
    }
});
React.render(
    <Layout>

    </Layout>,
    document.getElementById('app')
);
