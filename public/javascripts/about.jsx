"use strict";
let About = React.createClass({
    getInitialState: function () {
        return {data: []};
    },

    componentDidMount: function () {
        $.get(this.props.url, function (data) {
            if (this.isMounted()) {
                this.setState({data: data});
            }
        }.bind(this));
    },

    render: function () {
        return (
            <div className="about">
                <p className="about_text">Valentina Styles is a newly established store which prides itself on having a wide range
                    of high street fashion attire for women. It sells in the following categories; Smart Suits,
                    Summer Dresses, Cocktail Party, Evening Wear and Accessories such as sunglasses and
                    clutch purse. The collection ranges from medium-priced ($100-$300) to expensive
                    ($500+).</p>

                <p className="about_text">Why Valentina?<br/>
                    Lower pricebr <br/>
                    Higher quality</p>
            </div>
        );
    }
});
React.render(
    <Layout>
        <About url="data/products.json"/>
    </Layout>,
    document.getElementById('app')
);