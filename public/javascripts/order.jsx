"use strict";
let Order = React.createClass({
    getInitialState: function () {
        return {data: []};
    },

    componentDidMount: function () {
        $.get(this.props.url, function (data) {
            if (this.isMounted()) {
                this.setState({data: data});
            }
        }.bind(this));
    },

    render: function () {
        return (
            <div className="order">
                <table border="1" cellPadding="5px" cellspacing="1px">
                    <tr bgcolor="#FFFFFF">
                        <th>Order Id</th>
                        <th>Total Amount</th>
                        <th>CreateTime</th>
                        <th>Status</th>
                    </tr>
                    {this.state.data.map(function (item) {
                        return (

                            <tbody>
                            <tr bgcolor="#FFFFFF">
                                <td>{item.orderId}</td>
                                <td>{item.totalAmount}</td>
                                <td>{item.createTime}</td>
                                <td>{item.status}</td>
                            </tr>
                            <tr>
                                <td colSpan="8">
                                    <table border="1" className="productsList">
                                        <tbody>
                                        <tr>
                                            <th>Name</th>
                                            <th>Price</th>
                                            <th>Category</th>
                                            <th>Quantity</th>
                                        </tr>
                                        {item.details.map(function(detail){
                                            return (<tr>
                                                <td>{detail.name}</td>
                                                <td>${detail.price}</td>
                                                <td>{detail.category}</td>
                                                <td>{detail.quantity}</td>
                                            </tr>);
                                        })}

                                        </tbody>
                                    </table>
                                </td>
                            </tr>

                            </tbody>


                        )
                    })}

                </table>
            </div>
        );
    }
});
React.render(
    <Layout>
        <Order url="data/order.json"/>
    </Layout>,
    document.getElementById('app')
);


