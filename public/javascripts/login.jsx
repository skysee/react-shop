var Login = React.createClass({
    getInitialState: function () {
        return {data: []};
    },

    componentDidMount: function () {
        $.get(this.props.url, function (data) {
            if (this.isMounted()) {
                this.setState({data: data});
            }
        }.bind(this));
    },

    render: function () {
        return (
            <div className="login">
                <form action="/Customer/Login">
                    <table>
                        {this.state.data.map(function (item) {
                            return (
                                <tr>
                                    <td className="login_name">{item.title} &nbsp;</td>
                                    <td><input className="login_input" type="text" name={item.name}/></td>
                                </tr>)
                        })}
                        <tr>
                            <td></td>
                            <td><input type="submit" className="login_submit" value="Submit"/></td>
                        </tr>
                    </table>
                </form>
            </div>
        );
    }
});
React.render(
    <Login url="data/login.json"/>,
    document.getElementById('login')
);

