"use strict";
let FeedbackForm = React.createClass({
    getInitialState: function () {
        return {data: []};
    },

    componentDidMount: function () {
        $.get(this.props.url, function (data) {
            if (this.isMounted()) {
                this.setState({data: data});
            }
        }.bind(this));
    },

    handle_change: function (e) {
        if (e.target.value.length == 0) {
            alert("Please input " + e.target.name);

        }
    },
    empty: function (elem, msg) {
        if (elem.value.length == 0) {
            alert(msg);
            return true;
        }
        return false;
    },

    handleSubmit: function () {

        var name = document.getElementById('name');
        var phone = document.getElementById('phone');
        var email = document.getElementById('email');
        var message = document.getElementById('feedback');


        if (this.empty(name, "please enter your name")) {
            return false;
        }
        if (this.empty(phone, "please enter your phone")) {
            return false;
        }

        if (this.empty(email, "please enter your email")) {
            return false;
        }
        if (this.empty(feedback, "please enter your feedback")) {
            return false;
        }
        return true;
    },
    render: function () {
        return (
            <div>
                <div className="feedback_form">
                    <h4>Fill Your Feedback</h4>

                    <form action="">
                        <input type="text" placeholder="Your Name" name="name" id="name"/><br/>
                        <input type="text" placeholder="Your Phone" name="phone" id="phone"/><br/>
                        <input type="text" placeholder="Your Email" name="email" id="email"/><br/>
                        <textarea rows="5" cols="38" placeholder="Your Feedback" name="feedback"
                                  id="feedback"></textarea><br/>
                        <input type="submit" className="contact_submit" value="submit"/>
                    </form>
                </div>
                <div className="feedback_help">
                    <h4>Feedback</h4>

                    <p>
                        Customers must register successfully in order to buy a product. It is very important you

                        as the developer put appropriate validations on all forms created (client/server side

                        validations). Invalid data must not be passed onto the server. Only after successful

                        registration, a customer can login. Once they login they can start adding products in their

                        shopping cart and proceed onto their final checkout and payments. Having a fully

                        functional shopping cart is an integral part of the prototype. For this case study, billing

                        functionality such as credit card payments can be excluded.<br/><br/>

                        A key requirement by Valentina is that registered customers must be able to make

                        product recommendations that are directly displayed on the website for every user. Once

                        a customer registers he can view/update his profile and also add a recommendation.

                        Another key requirement is having a Search Function, whereby customers must be able

                        to search for available products (you may use product name or the price range as a

                        filter). Most importantly, customers must be able to view their order history after login.

                        It is also vital that the Contact Us page has an enquiry form for product enquiry or any

                        other matter.
                    </p>

                </div>

            </div>
        );
    }
});
React.render(
    <Layout>
        <FeedbackForm url="data/feedback.json"/>
    </Layout>,
    document.getElementById('app')
);