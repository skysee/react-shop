var ProductList = React.createClass({
    getInitialState: function () {
        return {data: []};
    },

    componentDidMount: function () {

        var pos = location.href.indexOf("?");
        var query;
        if (pos != -1) {
            query = location.href.substr(pos + 1);

        }
        $.get(this.props.url + query, function (data) {
            if (this.isMounted()) {
                this.setState({data: data});
            }
        }.bind(this));
    },
    handleClick: function(id){
        window.location.href="/Order/AddToCart?product="+id;
    },
    render: function () {

        return (
            <div className="products">
                {this.state.data.map(function (item) {
                    console.log(this);
                    return (
                        <div className="productList">
                            <img src={item.src} alt={item.alt}/>
                            <p className="price">Price:{item.price}</p>
                            <button onClick={this.handleClick.bind(this,item.id)}>Buy Online</button>
                        </div>
                    );
                },this)}
            </div>
        );
    }
});


React.render(
    <Layout>
        <ProductList url="data/products.json?"/>
    </Layout>,
    document.getElementById('app')
);