"use strict";
let Registration = React.createClass({
    getInitialState: function () {
        return {data: []};
    },

    componentDidMount: function () {
        $.get(this.props.url, function (data) {
            if (this.isMounted()) {
                this.setState({data: data});
            }
        }.bind(this));
    },

    handle_change: function (e) {
        if (e.target.value.length == 0) {
            alert("Please input " + e.target.name);

        }
    },
    empty: function (elem, msg) {
        if (elem.value.length == 0) {
            alert(msg);
            return true;
        }
        return false;
    },

    handleSubmit: function () {

        var title = document.getElementById('title');
        var firstName = document.getElementById('firstName');
        var lastName = document.getElementById('lastName');
        var gender = document.getElementById('gender');
        var dob = document.getElementById('dob');
        var email = document.getElementById('email');
        var password = document.getElementById('password');

        if (this.empty(email, "please enter your email")) {
            return false;
        }
        if (this.empty(password, "please enter your password")) {
            return false;
        }

        if (this.empty(title, "please enter your title")) {
            return false;
        }
        if (this.empty(firstName, "please enter your firstName")) {
            return false;
        }
        if (this.empty(lastName, "please enter your lastName")) {
            return false;
        }
        if (this.empty(gender, "please enter your gender")) {
            return false;
        }
        if (this.empty(dob, "please enter your dob")) {
            return false;
        }
        return true;
    },
    render: function () {
        return (
            <div>
                <div className="register_form">
                    <form className="form" onSubmit={this.handleSubmit} action="/Customer/Create" method="POST">
                        <p className="form-legend">Personal Information</p>
                        <table>
                            <tbody>
                            {this.state.data.map(function (item) {
                                return <tr className="form_tr">
                                    <td className="form_text">{item.title}</td>
                                    <td className="form_td">
                                        <input
                                            className="form_input"
                                            id={item.name}
                                            ref={item.name}
                                            name={item.name}
                                            type={item.type}
                                            onChange={this.handle_change}/>
                                    </td>
                                </tr>;
                            }, this)}
                            </tbody>
                        </table>
                        <input type="submit" value="submit" className="submit"/>
                    </form>
                </div>


                <div className="register_help">
                    <h4>Valentina Styles</h4>

                    <p>
                        Valentina Styles is a newly established store which prides itself on having a wide range

                        of high street fashion attire for women. It sells in the following categories; Smart Suits,

                        Summer Dresses, Cocktail Party, Evening Wear and Accessories such as sunglasses and

                        clutch purse. The collection ranges from medium-priced ($100-$300) to expensive

                        ($500+).
                    </p>

                </div>
            </div>
        );
    }
});
React.render(
    <Layout>
        <Registration url="data/registration.json"/>
    </Layout>,
    document.getElementById('app')
);