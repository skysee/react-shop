var Header = React.createClass({
    getInitialState: function () {
        return {data: []};
    },

    componentDidMount: function () {
        $.get(this.props.url, function (data) {
            if (this.isMounted()) {
                this.setState({data: data});
            }
        }.bind(this));
    },

    render: function () {
        var loginButton;
        if (this.state.data.login) {
            loginButton = <Logout/>;
        } else {
            loginButton = <Login/>;
        }
        return (
            <div id="header">
                <div className="header">
                    <a href="index.html" className="logo">
                        <img src="images/logo.png" className="logo_img"/>
                    </a>

                    <p className="logo_text"> Valentina Style</p>
                    {loginButton}
                </div>
            </div>
        );
    }
});

var Login = React.createClass({
    getInitialState: function () {
        return {data: []};
    },

    componentDidMount: function () {
        $.get(this.props.url, function (data) {
            if (this.isMounted()) {
                this.setState({data: data});
            }
        }.bind(this));
    },

    render: function () {
        return (
            <div className="login">
                <form action="/Customer/Login">
                    <table>

                        <tr>
                            <td className="login_name">User Name: &nbsp;</td>
                            <td><input className="login_input" type="text" name="name"/></td>
                        </tr>
                        <tr>
                            <td className="login_name">Password: &nbsp;</td>
                            <td><input className="login_input" type="text" name="password"/></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="submit" className="login_submit" value="Submit"/></td>
                        </tr>
                    </table>
                </form>
            </div>
        );
    }
});

var Logout = React.createClass({
    getInitialState: function () {
        return {data: []};
    },

    componentDidMount: function () {
        $.get(this.props.url, function (data) {
            if (this.isMounted()) {
                this.setState({data: data});
            }
        }.bind(this));
    },

    render: function () {
        return (
            <div className="login">
                <form action="/Customer/Logout">
                    <table>
                        <tr>
                            <td></td>
                            <td><input type="submit" className="login_submit" value="Logout"/></td>
                        </tr>
                    </table>
                </form>
            </div>
        );
    }
});

var DropDown = React.createClass({
    getInitialState: function () {
        return {data: []};
    },

    componentDidMount: function () {
        $.get(this.props.url, function (data) {
            if (this.isMounted()) {
                this.setState({data: data});
            }
        }.bind(this));
    },

    handleClick: function(id){
        window.location.href="/products.html?category_id="+id;
    },
    render: function () {
        return (
            <div className="nav_bar_top" id="nav_bar_top">
                <ul className="nav_bar_top_ul" id="nav_bar_top_ul">
                    {this.state.data.map(function (item) {
                        return (
                            <li className="nav_bar_top_li" id="nav_bar_top_li">
                                <a href={item.href} className="nav_bar_top_li_a">{item.content}</a>
                                <ul className="nav_bar_top_sub">
                                    {item.children.map(function (child) {
                                        return (
                                            <li onClick={this.handleClick.bind(this,child.id)}
                                                className="nav_bar_top_sub_li" id="nav_bar_top_sub_li_select">
                                                {child.subContent}
                                            </li>)
                                    },this)}
                                </ul>
                            </li>
                        );
                    },this)}
                </ul>
            </div>
        )
    }
});



var MenuLeft = React.createClass({
    getInitialState: function () {
        return {data: []};
    },

    componentDidMount: function () {
        $.get(this.props.url, function (data) {
            if (this.isMounted()) {
                this.setState({data: data});
            }
        }.bind(this));
    },

    handleClick: function(id){
        window.location.href="/products.html?category_id="+id;
    },
    render: function () {
        return (
            <div className="nav_bar_left" id="nav_bar_left">
                <ul className="nav_bar_left_ul">

                    {this.state.data.map(function (item) {
                        console.log(this);

                        return <li className="nav_bar_left_li" onClick={this.handleClick.bind(this,item.id)}>{item.content}</li>;
                    },this)}
                </ul>
            </div>
        );
    }
});

var Footer = React.createClass({
    render: function () {
        return (
            <div id="footer">
                <div className="footer">
                    <p className="copyright">Copyright@Leon Zhou 2015</p>.
                </div>
            </div>
        );
    }
});

var Layout = React.createClass({

    render: function () {
        return (
            <div>
                <Header url="data/login.json"/>
                <DropDown url="data/nav_bar_top_sub.json"/>

                <div className="content">
                    <MenuLeft url="data/nav_bar_left_ul.json"/>
                    {this.props.children}
                </div>
                <div className="clear"></div>
                <Footer/>

            </div>
        );
    }
});


