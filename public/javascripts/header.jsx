var Header = React.createClass({
    getInitialState: function () {
        return {data: []};
    },

    componentDidMount: function () {
        $.get(this.props.url, function (data) {
            if (this.isMounted()) {
                this.setState({data: data});
            }
        }.bind(this));
    },

    render: function () {
        return (
            <div className="header">
                <a href="index.html" className="logo">
                    <img src="images/logo.png" className="logo_img"/>
                </a>

                <p className="logo_text"> Valentina Style</p>
                <div id="login"/>
            </div>

        );
    }
});
React.render(
    <Header />,
    document.getElementById('header')
);
