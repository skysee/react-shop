"use strict";
let ContactForm = React.createClass({
    getInitialState: function () {
        return {data: []};
    },

    componentDidMount: function () {
        $.get(this.props.url, function (data) {
            if (this.isMounted()) {
                this.setState({data: data});
            }
        }.bind(this));
    },

    handle_change: function (e) {
        if (e.target.value.length == 0) {
            alert("Please input " + e.target.name);

        }
    },
    empty: function (elem, msg) {
        if (elem.value.length == 0) {
            alert(msg);
            return true;
        }
        return false;
    },

    handleSubmit: function () {

        var name = document.getElementById('name');
        var phone = document.getElementById('phone');
        var email = document.getElementById('email');
        var message = document.getElementById('message');


        if (this.empty(name, "please enter your name")) {
            return false;
        }
        if (this.empty(phone, "please enter your phone")) {
            return false;
        }

        if (this.empty(email, "please enter your email")) {
            return false;
        }
        if (this.empty(message, "please enter your message")) {
            return false;
        }
        return true;
    },
    render: function () {
        return (
            <div>
                <div className="contact_form">
                    <h4>Fill Your Details</h4>

                    <form action="">
                        <input type="text" placeholder="Your Name" name="name" id="name"/><br/>
                        <input type="text" placeholder="Your Phone" name="phone" id="phone"/><br/>
                        <input type="text" placeholder="Your Email" name="email" id="email"/><br/>
                        <textarea rows="5" cols="38" placeholder="Your Message" name="message"
                                  id="message"></textarea><br/>
                        <input type="submit" className="contact_submit" value="submit"/>
                    </form>
                </div>

            </div>
        );
    }
});


var ExampleGoogleMap = React.createClass({
    getDefaultProps: function () {
        return {
            initialZoom: 14,
            mapCenterLat: -36.801060,
            mapCenterLng: 174.747563,
        };
    },
    componentDidMount: function (rootNode) {
        var mapOptions = {
                center: this.mapCenterLatLng(),
                zoom: this.props.initialZoom
            },
            map = new google.maps.Map(this.getDOMNode(), mapOptions);
        var marker = new google.maps.Marker({position: this.mapCenterLatLng(), title: 'Hi', map: map});
        this.setState({map: map});
    },
    mapCenterLatLng: function () {
        var props = this.props;
        return new google.maps.LatLng(props.mapCenterLat, props.mapCenterLng);
    },
    render: function () {
        return (
            <div className="google_map">
                <div className='map-gic'></div>
            </div>
        );
    }
});


/*-----------------------------*/
React.render(
    <Layout>
        <ContactForm url="data/contact.json"/>
        <ExampleGoogleMap />
    </Layout>,
    document.getElementById('app')
);