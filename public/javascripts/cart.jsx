var Registration = React.createClass({
    getInitialState: function () {
        return {data: []};
    },

    loadData: function () {
        $.get(this.props.url, function (data) {
            if (this.isMounted()) {
                this.setState({data: data});
            }
        }.bind(this));
    },
    componentDidMount: function () {
        this.loadData();
    },

    clear: function () {
        window.location.href = '/Order/Clear';
    },
    update: function () {
        document.getElementById("form").submit();
    },
    order: function () {
        window.location.href = '/Order/Place';
    },
    shopping: function () {
        window.location.href = '/products.html';
    },
    remove: function (id) {
        window.location.href = '/Order/Delete/' + id;
    },
    //https://facebook.github.io/react/tips/communicate-between-components.html
    //http://stackoverflow.com/questions/21010400/how-to-set-event-handler-in-react-sub-component
    render: function () {
        return (
            <div className="cart_form">

                <table className="carttable" border="1" cellPadding="5px" cellSpacing="1px">
                    <form id="form" method="POST" action="/Order/Update">

                    <thead>
                    <tr bgcolor="#FFFFFF">
                        <td>Id</td>
                        <td>Name</td>
                        <td>Price</td>
                        <td>Qty</td>
                        <td>Amount</td>
                        <td>Options</td>
                    </tr>
                    {this.state.data.map(function (item) {
                        return (
                            <tr bgcolor="#FFFFFF">
                                <td>{item.id}</td>
                                <td>{item.name}</td>
                                <td>$ {item.price}</td>
                                <td><input type="text" name={item.id} value={item.qty}/></td>
                                <td>${item.price * item.qty}</td>
                                <td><input type="button" value="Remove" onClick={this.remove.bind(this,item.id)}/></td>
                            </tr>
                        )
                    }, this)}
                    </thead>
                    </form>

                    <tbody>

                    <tr>
                        <td><b>Order Total:$0</b></td>
                        <td colSpan="5" align="right">

                            <input type="button" value="Continue Shopping" onClick={this.shopping}/>

                            <input type="button" value="Clear Cart" onClick={this.clear}/>

                            <input type="button" value="Update Cart" onClick={this.update}/>

                            <input type="button" value="Place Order" onClick={this.order}/>

                        </td>
                    </tr>
                    </tbody>
                </table>

            </div>
        );
    }
});
React.render(
    <Layout>
        <Registration url="data/cart.json"/>
    </Layout>,
    document.getElementById('app')
);


