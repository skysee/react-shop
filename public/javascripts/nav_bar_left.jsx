var MenuLeft = React.createClass({
    getInitialState: function () {
        return {data: []};
    },

    componentDidMount: function () {
        $.get(this.props.url, function (data) {
            if (this.isMounted()) {
                this.setState({data: data});
            }
        }.bind(this));
    },

    render: function () {
        return (
            <ul className="nav_bar_left_ul">
                {this.state.data.map(function (item) {
                    return <li className="nav_bar_left_li">{item.content}</li>;
                })}
            </ul>
        );
    }
});
React.render(
    <MenuLeft url="data/nav_bar_left_ul.json"/>,
    document.getElementById('nav_bar_left')
);


