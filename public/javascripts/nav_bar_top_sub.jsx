var DropDown = React.createClass({
    getInitialState: function () {
        return {data: []};
    },

    componentDidMount: function () {
        $.get(this.props.url, function (data) {
            if (this.isMounted()) {
                this.setState({data: data});
            }
        }.bind(this));
    },


    render: function () {
        return (
            <ul className="nav_bar_top_ul" id="nav_bar_top_ul">
                {this.state.data.map(function (item,index) {
                    console.log(item);
                    console.log(index);
                    return (
                        <li className="nav_bar_top_li" id="nav_bar_top_li">
                            <a href={item.href} className="nav_bar_top_li_a">{item.content}</a>
                            <ul className="nav_bar_top_sub">
                                {item.children.map(function (child) {
                                    return (
                                        <li className="nav_bar_top_sub_li" id="nav_bar_top_sub_li_select">
                                            <a href={child.subHref}>{child.subContent}</a>
                                        </li>)
                                })}
                            </ul>
                        </li>
                    );
                })}
            </ul>
        )
    }
});
React.render(
    <DropDown url="data/nav_bar_top_sub.json"/>,
    document.getElementById('nav_bar_top')
);
