<!DOCTYPE html>
<html>

<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <link href="../web/css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
<div id="header">

</div>

<div id="container">
    {include file='../backView/menu.tpl'}

    <div id="container_right">
        <div id="container_right_update">
            <p id="update_title">Update form</p>

            <form id="update_form" action="../backController/orderUpdate.php" method="post" enctype="multipart/form-data">

                <div class="update_table_con">
                    <table id="update_table" border="0">
                        <input type="hidden" name="id" value="{$order->id}"/>

                        <tr>
                            <td>Status:</td>
                            <td>
                                <select name="status_id">
                                    {foreach from=$statuses item=status}
                                    <option {if $status->id == $order->status->id} selected="selected" {/if}
                                        value="{$status->id}" >{$status->name}
                                    </option>
                                    {/foreach}
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="clear: both"></div>
                <div class="update_submit">
                    <input type="submit" value="submit" id="update_submit" name="update_submit"/>
                </div>
            </form>
        </div>
    </div>
    <div style="clear: both"></div>
</div>
</body>
</html>