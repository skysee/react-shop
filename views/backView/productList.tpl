<!DOCTYPE html>
<html>

<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <link href="../web/css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
<div id="header">

</div>

<div id="container">
    {include file='../backView/menu.tpl'}

    <div id="container_right">
        <div id="container_right_search">
            <form class="customer_search" method="post" action="../backController/productList.php">
                product's name <input type="text" size="20" id="name" name="name"/> <br/>
                <input type="submit" value="search"/>
            </form>
        </div>
        <div id="container_right_add">
            <form class="customer_search">
                <a href="../backController/productAdd.php">
                    <input type="button" value="Add" id="myAdd" name="myAdd"></a>
            </form>
        </div>
        <div id="container_right_table">
            <table class="table" border="1">
                <tr>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Category</th>
                    <th>IS HOT</th>
                    <th style="text-align: center" colspan="2">Operations</th>
                </tr>

                {foreach from=$products item=product}
                    <tr>
                        <td>{$product->name}</td>
                        <td>{$product->price}</td>
                        <td>{$product->category->name}</td>
                        <td>
                            {if $product->is_hot}
                                YES
                            {else}
                                NO
                            {/if}

                        <td>
                            <button><a href="../backController/productDelete.php?id={$product->id}">Delete</a></button>
                        </td>
                        <td>
                            <button><a href="../backController/productEdit.php?id={$product->id}">Update</a></button>
                        </td>
                    </tr>
                {/foreach}

            </table>
        </div>
    </div>
    <div style="clear: both"></div>
</div>
</body>
</html>