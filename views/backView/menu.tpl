<div id="container_left">
    <ul id="menu" type="none">
        <li><a href="../backController/productList.php" class="node">Product Management</a></li>
        <li><a href="../backController/orderList.php" class="node">Order Management</a></li>
        <li><a href="../backController/recordEdit.php" class="node">Record Management</a></li>
        {if $logged_in}
            <li><a href="../backController/recordEdit.php" class="node">Staff Management</a></li>
        {/if}
        <li><a href="../backController/logout.php" class="node">Logout</a></li>
    </ul>
</div>