<!DOCTYPE html>
<html>

<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <link href="../web/css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
<div id="header">

</div>

<div id="container">
    {include file='../backView/menu.tpl'}

    <div id="container_right">
        <div id="container_right_update">
            <p id="update_title">Update form</p>

            <form id="update_form" action="../backController/recordUpdate.php" method="post" enctype="multipart/form-data">

                <div class="update_table_con">
                    <table id="update_table" border="0">
                        <input type="hidden" name="id" value="{$staff->id}"/>

                        <tr>
                            <td>Email:</td>
                            <td><input type="text" size="20" name="email" value="{$staff->email}"><br/>
                            </td>
                        </tr>
                        <tr>
                            <td>FirstName:</td>
                            <td><input type="text" size="20" name="firstname" value="{$staff->firstname}"><br/>
                            </td>
                        </tr>
                        <tr>
                            <td>LastName:</td>
                            <td><input type="text" size="20" name="lastname" value="{$staff->lastname}"><br/>
                            </td>
                        </tr>
                        <tr>
                            <td>Password:</td>
                            <td><input type="text" size="20" name="password" value="{$staff->password}"><br/>
                            </td>
                        </tr>
                    </table>

                </div>
                <div style="clear: both"></div>
                <div class="update_submit">
                    <input type="submit" value="submit" id="update_submit" name="update_submit"/>
                </div>
            </form>
        </div>
    </div>
    <div style="clear: both"></div>
</div>
</body>
</html>