<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title></title>
    <link href="../web/css/style.css" type="text/css" rel="stylesheet">
    <script src="../web/js/jquery.min.js" type="text/javascript"></script>
    <script>

        $(document).ready(function () {
            $("[id^='subTable_']").hide();
            $("[id^='order_']").click(function () {
                var id = $(this).attr("id");
                id = id.substr(id.indexOf("order") + "order".length + 1);
                $("#subTable_" + id).toggle();
                if ($("#subTable_" + id).is(':hidden')) {
                    $(this).text("+");
                } else {
                    $(this).text("-");
                }
            });
        });
    </script>
</head>
<body>
<div id="header">

</div>

<div id="container">
    {include file='../backView/menu.tpl'}

    <div id="container_right">
        <div id="container_right_table">
            <table class="table" border="1">
                <tr>
                    <th></th>
                    <th>Order Id</th>
                    <th>Customer's Name</th>
                    <th>Total Amount</th>
                    <th>Create Time</th>
                    <th>Status</th>
                    <th colspan="2">Operations</th>
                </tr>


                {foreach from=$orders item=order}
                    <tr>
                        <td id="order_{$order->id}">+</td>
                        <td>{$order->transnum}</td>
                        <td>{$order->customer->firstname}</td>
                        <td>{$order->totalamt}</td>
                        <td>{$order->createtime|date_format:'Y-m-d H:i:s'}</td>
                        <td>{$order->status->name}</td>
                        <td>
                            <button><a href="../backController/orderEdit.php?id={$order->id}">Edit</a></button>
                            <button><a href="../backController/orderDelete.php?id={$order->id}">Delete</a></button>
                        </td>
                    </tr>
                    <tbody id=subTable_{$order->id}>
                    <tr>
                        <td colspan="7">
                            <table class="subTable" border="1">
                                <tr>
                                    <th>Name</th>
                                    <th>Price </th>
                                    <th>Category</th>
                                    <th>Quantity</th>
                                    <th>Operations</th>
                                </tr>
                                {foreach from=$order->product item=product}
                                    <tr>
                                        <td>{$product->name} </td>
                                        <td>{$product->price} </td>
                                        <td>{$product->category->name} </td>
                                        <td>{$order->getQty($product->id)} </td>
                                        <td>
                                            <button>
                                                <a href="../backController/productEdit.php?id={$product->id}">View</a>
                                            </button>
                                        </td>
                                    </tr>
                                {/foreach}
                            </table>
                        </td>
                    </tr>
                    </tbody>
                {/foreach}
            </table>
        </div>
    </div>
    <div style="clear: both"></div>
</div>
</body>
</html>
