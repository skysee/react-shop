<!DOCTYPE html>
<html>

<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <link href="../web/css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
<div id="header">

</div>

<div id="container">
    {include file='../backView/menu.tpl'}


    <div id="container_right">
        <div id="container_right_update">
            <p id="update_title">Add form</p>

            <form id="update_form" action="../backController/productCreate.php" method="post" enctype="multipart/form-data">
                <div class="update_table_con">
                    <table id="update_table" border="0">
                        <tr>
                            <td>Name:</td>
                            <td><input type="text" size="20" id="update_name" name="name"><br/>
                            </td>
                        </tr>
                        <tr>
                            <td>Price:</td>
                            <td><input type="text" size="20" id="update_price" name="price"><br/>
                            </td>
                        </tr>
                        <tr>
                            <td>Description:</td>
                            <td>
                                <textarea id="update_description" name="description" cols="12" rows="6"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>IS HOT:</td>
                            <td>
                                <select name="is_hot">
                                        <option value="1">YES</option>
                                        <option value="0">NO</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <table id="update_table_2" border="0">
                        <tr>
                            <td>Category:</td>
                            <td>
                                <select name="category_id">
                                    {foreach from=$categories item=category}
                                    <option value="{$category->id}">{$category->name}</option>
                                    {/foreach}
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Picture:</td>
                            <td>
                                <input type="file" name="fileToUpload" id="fileToUpload">
                            </td>
                        </tr>
                    </table>
                    <div class="update_submit">
                        <input type="submit" value="submit" id="update_submit" name="update_submit"/>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div style="clear: both"></div>
</div>
</body>
</html>