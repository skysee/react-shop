var Layout = React.createClass({displayName: "Layout",

    render: function () {
        return (
            React.createElement("div", null, 
                React.createElement("div", {id: "header"}), 
                React.createElement("div", {className: "nav_bar_top", id: "nav_bar_top"}), 
                React.createElement("div", {className: "content"}, 
                    React.createElement("div", {className: "nav_bar_left", id: "nav_bar_left"}), 
                    this.props.children

                ), 
                React.createElement("div", {className: "clear"}), 
                React.createElement("div", {id: "footer"})

            )
        );
    }
});
React.render(
    React.createElement(Layout, null

    ),
    document.getElementById('app')
);
