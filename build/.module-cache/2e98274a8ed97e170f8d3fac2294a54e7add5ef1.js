var Footer = React.createClass({displayName: "Footer",
    render: function() {
        return (
            React.createElement("div", {className: "footer"}, 
                React.createElement("p", {className: "copyright"}, "Copyright@Leon Zhou 2015"), "."
            )
        );
    }
});

React.render(
    React.createElement(Footer, null),
    document.getElementById('footer')
);