var Header = React.createClass({displayName: "Header",
    getInitialState: function () {
        return {data: []};
    },

    componentDidMount: function () {
        $.get(this.props.url, function (data) {
            if (this.isMounted()) {
                this.setState({data: data});
            }
        }.bind(this));
    },

    render: function () {
        return (
            React.createElement("div", {className: "header"}, 
                React.createElement("a", {href: "index.html", className: "logo"}, 
                    React.createElement("img", {src: "images/logo.png", className: "logo_img"})
                ), 

                React.createElement("p", {className: "logo_text"}, " Valentina Style"), 
                React.createElement("div", {id: "login"})
            )

        );
    }
});
React.render(
    React.createElement(Header, null),
    document.getElementById('header')
);
